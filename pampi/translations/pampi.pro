SOURCES      += ../libs/plugins/anim_rotate_y_infinite.py
SOURCES      += ../libs/plugins/ui_title.py
SOURCES      += ../libs/plugins/image.py
SOURCES      += ../libs/plugins/ui_video.py
SOURCES      += ../libs/plugins/anim_scale.py
SOURCES      += ../libs/plugins/audio.py
SOURCES      += ../libs/plugins/bold.py
SOURCES      += ../libs/plugins/ui_anim.py
SOURCES      += ../libs/plugins/plugin_rc.py
SOURCES      += ../libs/plugins/_font_template.py
SOURCES      += ../libs/plugins/underlined.py
SOURCES      += ../libs/plugins/anim_rotate_y.py
SOURCES      += ../libs/plugins/_template.py
SOURCES      += ../libs/plugins/video.py
SOURCES      += ../libs/plugins/tt.py
SOURCES      += ../libs/plugins/title.py
SOURCES      += ../libs/plugins/ui_audio.py
SOURCES      += ../libs/plugins/italic.py
SOURCES      += ../libs/plugins/anim_rotate_z.py
SOURCES      += ../libs/plugins/_anim_template.py
SOURCES      += ../libs/plugins/anim_rotate_x.py

SOURCES      += ../pampi.pyw

SOURCES      += ../libs/main.py
SOURCES      += ../libs/utils.py
SOURCES      += ../libs/utils_about.py
SOURCES      += ../libs/utils_filesdirs.py
SOURCES      += ../libs/utils_functions.py
SOURCES      += ../libs/utils_pdf.py
SOURCES      += ../libs/utils_wizard.py
SOURCES      += ../libs/ui_main.py

TRANSLATIONS += ../translations/pampi.ts
TRANSLATIONS += ../translations/pampi_fr.ts
