<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr" sourcelanguage="">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../libs/plugins/ui_video.py" line="133"/>
        <source>Video parameters</source>
        <translation>Paramètres vidéo</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_video.py" line="55"/>
        <source>Video:</source>
        <translation type="obsolete">Vidéo :</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_audio.py" line="129"/>
        <source>Identifier:</source>
        <translation>Indentifiant :</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_video.py" line="139"/>
        <source>Poster Image</source>
        <translation>Image de présentation</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_video.py" line="134"/>
        <source>Video</source>
        <translation>Vidéo</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_video.py" line="138"/>
        <source>Choose the poster image</source>
        <translation>Choix de l&apos;image de présentation</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_video.py" line="135"/>
        <source>Video properties</source>
        <translation>Propriétés de la vidéo</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_video.py" line="140"/>
        <source>Video starters</source>
        <translation>Départs vidéo</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_video.py" line="128"/>
        <source>Start time</source>
        <translation type="obsolete">Instant (s)</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_audio.py" line="132"/>
        <source>Add starter</source>
        <translation>Ajout départ</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_audio.py" line="131"/>
        <source>Starter&apos;s name</source>
        <translation>Nom du départ</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_audio.py" line="126"/>
        <source>Audio parameters</source>
        <translation>Paramètres audio</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_audio.py" line="127"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_audio.py" line="128"/>
        <source>Audio properties</source>
        <translation>Propriétés audio</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_audio.py" line="130"/>
        <source>Audio starters</source>
        <translation>Départs audio</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_anim.py" line="60"/>
        <source>Animation settings</source>
        <translation>Réglages de l&apos;animation</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_anim.py" line="62"/>
        <source>Animation range</source>
        <translation>Portée de l&apos;animation</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_anim.py" line="63"/>
        <source>Global</source>
        <translation>Globale</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_anim.py" line="64"/>
        <source>Text only</source>
        <translation>Texte seulement</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_anim.py" line="65"/>
        <source>div</source>
        <translation>div</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_anim.py" line="61"/>
        <source>class=&quot;anim-rotate-x&quot;</source>
        <translation>class=&quot;anim-rotate-x&quot;</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_anim.py" line="67"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_anim.py" line="66"/>
        <source>Animation contents</source>
        <translation>Contenus à animer</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_title.py" line="73"/>
        <source>Make a title</source>
        <translation>Créer un titre</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_title.py" line="74"/>
        <source>Title category</source>
        <translation>Catégorie de titre</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_title.py" line="75"/>
        <source>Level 1</source>
        <translation>Niveau 1</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_title.py" line="76"/>
        <source>Level 2</source>
        <translation>Niveau 2</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_title.py" line="77"/>
        <source>Level 3</source>
        <translation>Niveau 3</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_title.py" line="78"/>
        <source>Level 4</source>
        <translation>Niveau 4</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_title.py" line="79"/>
        <source>Level 5</source>
        <translation>Niveau 5</translation>
    </message>
    <message>
        <location filename="../libs/plugins/ui_title.py" line="80"/>
        <source>Title contents</source>
        <translation>Texte du titre</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../libs/ui_main.py" line="330"/>
        <source>MainWindow</source>
        <translation>MainWindow</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="332"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="336"/>
        <source>CSS:</source>
        <translation>CSS :</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="339"/>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="373"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="342"/>
        <source>Tools:</source>
        <translation>Outils :</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="386"/>
        <source>Katex</source>
        <translation type="obsolete">Katex</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="387"/>
        <source>Vis</source>
        <translation type="obsolete">Vis</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="388"/>
        <source>D3</source>
        <translation type="obsolete">D3</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="389"/>
        <source>JSXGraph</source>
        <translation type="obsolete">JSXGraph</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="390"/>
        <source>Geogebra</source>
        <translation type="obsolete">Geogebra</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="382"/>
        <source>Cheat Sheet</source>
        <translation>Pense-bête</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="346"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="348"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="380"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="352"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="353"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="354"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="355"/>
        <source>Open file</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="356"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="362"/>
        <source>Presentation in full screen</source>
        <translation>Présentation en plein écran</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="358"/>
        <source>Open presentations dir</source>
        <translation>Ouvrir le dossier des présentations</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="376"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="359"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="360"/>
        <source>Save file</source>
        <translation>Enregistrer le fichier</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="361"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="366"/>
        <source>View in web browser</source>
        <translation>Ouvrir dans le navigateur</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="367"/>
        <source>Ctrl+B, Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="372"/>
        <source>Fullscreen</source>
        <translation type="obsolete">Plein écran</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="363"/>
        <source>Toggle fullscreen</source>
        <translation>Plein écran oui/non</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="364"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="375"/>
        <source>WebBrowser</source>
        <translation type="obsolete">Navigateu</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="368"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="347"/>
        <source>Recent Files</source>
        <translation>Fichiers récents</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="369"/>
        <source>Wizard</source>
        <translation>Assistant</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="370"/>
        <source>Save as...</source>
        <translation>Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="371"/>
        <source>Export the presentation</source>
        <translation>Exporter la présentation</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="372"/>
        <source>Create a launcher</source>
        <translation>Créer un lanceur</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="374"/>
        <source>Background color of presentation. Click to edit.</source>
        <translation>Couleur de fond de la présentation. Cliquer pour modifier.</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="375"/>
        <source>Save, convert and preview</source>
        <translation>Enregistrer, convertir et visualiser</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="365"/>
        <source>Open in the browser</source>
        <translation>Ouvrir dans le navigateur</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="349"/>
        <source>Other Tools</source>
        <translation>Autres outils</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="377"/>
        <source>Create PDF file</source>
        <translation>Créer un fichier PDF</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="357"/>
        <source>Open the presentation folder</source>
        <translation>Ouvrir le dossier des présentations</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="378"/>
        <source>Move the presentation folder</source>
        <translation>Déplacer le dossier des présentations</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="379"/>
        <source>Update the presentation folder</source>
        <translation>Mettre à jour le dossier des présentations</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="381"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="334"/>
        <source>Here you can define the title that will be displayed in the browser</source>
        <translation>Vous pouvez définir ici le titre qui sera affiché dans le navigateur</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="338"/>
        <source>If you are using a custom CSS file, enter its name here (without extension)</source>
        <translation>Si vous utilisez un fichier CSS personnalisé, indiquez ici son nom (sans extension)</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="341"/>
        <source>Some additional tools for your presentations</source>
        <translation>Quelques outils supplémentaires pour vos présentations</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="343"/>
        <source>Make the file editable to add your own examples</source>
        <translation>Rendre le fichier éditable pour ajouter vos propres exemples</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="345"/>
        <source>Edit Cheat Sheet</source>
        <translation>Modifier le pense-bête</translation>
    </message>
    <message>
        <location filename="../libs/ui_main.py" line="383"/>
        <source>A list of examples to copy-paste</source>
        <translation>Une liste d&apos;exemples à copier-coller</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../libs/main.py" line="319"/>
        <source>Exit</source>
        <translation type="obsolete">Quitter</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="526"/>
        <source>Help</source>
        <translation type="obsolete">Aide</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="85"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="483"/>
        <source>File</source>
        <translation type="obsolete">Fichier</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="88"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="93"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="62"/>
        <source>About {0}</source>
        <translation>À propos de {0}</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="165"/>
        <source>information message</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="166"/>
        <source>question message</source>
        <translation>Question</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="167"/>
        <source>warning message</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="168"/>
        <source>critical message</source>
        <translation>Message critique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="662"/>
        <source>Choose the Directory where the desktop file will be created</source>
        <translation>Choisissez le dossier où le lanceur sera créé</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="439"/>
        <source>Create a launcher</source>
        <translation type="obsolete">Créer un lanceur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="302"/>
        <source>The file has been modified.
Do you want to save your changes?</source>
        <translation>Le fichier a été modifié.
Voulez-vous enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="340"/>
        <source>Save</source>
        <translation type="obsolete">Enregistrer</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="208"/>
        <source>END !</source>
        <translation>TERMINÉ !</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="209"/>
        <source>Images are saved in the folder:</source>
        <translation>Les images sont enregistrées dans le dossier :</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="66"/>
        <source>(version {0})</source>
        <translation>(version {0})</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="442"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="557"/>
        <source>Save as...</source>
        <translation>Enregistrer sous...</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="360"/>
        <source>Save, convert and preview</source>
        <translation type="obsolete">Enregistrer, convertir et visualiser</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="367"/>
        <source>Convert to html with Pandoc</source>
        <translation type="obsolete">Convertir en html avec Pandoc</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="373"/>
        <source>Preview presentation in PAMPI</source>
        <translation type="obsolete">Visualiser la présentation dans PAMPI</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="390"/>
        <source>Open in the browser</source>
        <translation type="obsolete">Ouvrir dans le navigateur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="487"/>
        <source>Recent Files</source>
        <translation type="obsolete">Fichiers récents</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="502"/>
        <source>Tools</source>
        <translation type="obsolete">Outils</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="557"/>
        <source>Markdown Files (*.md)</source>
        <translation>Fichiers Markdown (*.md)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="553"/>
        <source>No name</source>
        <translation>Sans nom</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="421"/>
        <source>Open the presentation folder</source>
        <translation type="obsolete">Ouvrir le dossier des présentations</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="354"/>
        <source>Export the presentation</source>
        <translation type="obsolete">Exporter la présentation</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="592"/>
        <source>Select a directory to export the presentation</source>
        <translation>Sélectionnez un dossier pour exporter la présentation</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="710"/>
        <source>PANDOC IS NOT INSTALLED.</source>
        <translation>PANDOC N&apos;EST PAS INSTALLÉ.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="712"/>
        <source>This is the tool to convert Markdown files to html.</source>
        <translation>C&apos;est l&apos;outil qui permet de convertir les fichiers Markdown en html.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="714"/>
        <source>See the PAMPI help page:</source>
        <translation>Voir la page d&apos;aide de PAMPI :</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="249"/>
        <source>Select a directory for presentations</source>
        <translation>Sélectionnez un dossier pour vos présentations</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="178"/>
        <source>Title:</source>
        <translation type="obsolete">Titre :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="427"/>
        <source>Move the presentation folder</source>
        <translation type="obsolete">Déplacer le dossier des présentations</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="880"/>
        <source>Select a directory</source>
        <translation>Sélectionnez un dossier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="190"/>
        <source>CSS:</source>
        <translation type="obsolete">CSS :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="183"/>
        <source>Here you can define the title that will be displayed in the browser</source>
        <translation type="obsolete">Vous pouvez définir ici le titre qui sera affiché dans le navigateur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="195"/>
        <source>If you are using a custom CSS file, enter its name here (without extension)</source>
        <translation type="obsolete">Si vous utilisez un fichier CSS personnalisé, indiquez ici son nom (sans extension)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="458"/>
        <source>Cheat Sheet</source>
        <translation type="obsolete">Pense-bête</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="464"/>
        <source>A list of examples to copy-paste</source>
        <translation type="obsolete">Une liste d&apos;exemples à copier-coller</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="259"/>
        <source>Edit Cheat Sheet</source>
        <translation type="obsolete">Modifier le pense-bête</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="262"/>
        <source>Make the file editable to add your own examples</source>
        <translation type="obsolete">Rendre le fichier éditable pour ajouter vos propres exemples</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="470"/>
        <source>Wizard</source>
        <translation type="obsolete">Assistant</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="64"/>
        <source>File Creation Wizard</source>
        <translation>Assistant de création de fichier</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="90"/>
        <source>regular polygon</source>
        <translation>polygone régulier</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="91"/>
        <source>arrangement in table</source>
        <translation>disposition en tableau</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="92"/>
        <source>circular helix</source>
        <translation>hélice circulaire</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="100"/>
        <source>Presentation template:</source>
        <translation>Modèle de présentation :</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="111"/>
        <source>Number of steps (not counting the title):</source>
        <translation>Nombre d&apos;étapes (sans compter le titre) :</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="121"/>
        <source>Radius:</source>
        <translation>Rayon :</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="131"/>
        <source>Offset:</source>
        <translation>Décalage :</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="141"/>
        <source>Number of columns:</source>
        <translation>Nombre de colonnes :</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="151"/>
        <source>Number of rows:</source>
        <translation>Nombre de rangées :</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="186"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="190"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="93"/>
        <source>carousel</source>
        <translation>carrousel</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="66"/>
        <source>Select a presentation template and then use the available settings.</source>
        <translation>Sélectionnez un modèle de présentation puis utilisez les réglages disponibles.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="415"/>
        <source>Create PDF file</source>
        <translation type="obsolete">Créer un fichier PDF</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="115"/>
        <source>PDF export configuration</source>
        <translation>Configuration de l&apos;export PDF</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="136"/>
        <source>Print the notes</source>
        <translation>Imprimer les notes</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="118"/>
        <source>YES</source>
        <translation>OUI</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="120"/>
        <source>NO</source>
        <translation>NON</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="149"/>
        <source>Open the html file</source>
        <translation>Ouvrir le fichier html</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="162"/>
        <source>Create the PDF file</source>
        <translation>Créer le fichier PDF</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="172"/>
        <source>Copy only</source>
        <translation>Copier seulement</translation>
    </message>
    <message>
        <location filename="../libs/utils_wizard.py" line="174"/>
        <source>If you check this box, the result will be copied to the clipboard instead of replacing your current file</source>
        <translation>Si vous cochez cette case, le résultat sera copié dans le presse-papier au lieu de remplacer votre fichier actuel</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="202"/>
        <source>Color:</source>
        <translation type="obsolete">Couleur :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="398"/>
        <source>Background color of presentation. Click to edit.</source>
        <translation type="obsolete">Couleur de fond de la présentation. Cliquez pour modifier.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="977"/>
        <source>Select color</source>
        <translation>Sélectionnez une couleur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="326"/>
        <source>New</source>
        <translation type="obsolete">Nouveau</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="175"/>
        <source>PDF file name</source>
        <translation>Nom du fichier PDF</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="180"/>
        <source>Change</source>
        <translation>Changer</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="221"/>
        <source>pdf File</source>
        <translation>fichier PDF</translation>
    </message>
    <message>
        <location filename="../libs/utils_pdf.py" line="224"/>
        <source>pdf files (*.pdf)</source>
        <translation>fichiers pdf (*.pdf)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="208"/>
        <source>Tools:</source>
        <translation type="obsolete">Outils :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="511"/>
        <source>Other Tools</source>
        <translation type="obsolete">Autres outils</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="758"/>
        <source>Cannot read file {0}</source>
        <translation>Impossible d&apos;ouvrir le fichier {0}</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="212"/>
        <source>Some additional tools for your presentations</source>
        <translation type="obsolete">Quelques outils supplémentaires pour vos présentations</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="433"/>
        <source>Update the presentation folder</source>
        <translation type="obsolete">Mettre à jour le dossier des présentations</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="907"/>
        <source>This action will update the tools and examples provided with PAMPI.
Your personal files will not be deleted.

Do you want to continue ?</source>
        <translation>Cette action mettra à jour les outils et exemples fournis avec PAMPI.
Vos fichiers personnels ne seront pas effacés.

Voulez-vous continuer ?</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="379"/>
        <source>Presentation in full screen</source>
        <translation type="obsolete">Présentation en plein écran</translation>
    </message>
    <message>
        <location filename="../libs/plugins/image.py" line="25"/>
        <source>Insert an image</source>
        <translation>Insérer une image</translation>
    </message>
    <message>
        <location filename="../libs/plugins/_template.py" line="38"/>
        <source>Insert some template</source>
        <translation>Insère un exemple</translation>
    </message>
    <message>
        <location filename="../libs/plugins/_template.py" line="66"/>
        <source>Module name: {}</source>
        <translation type="obsolete">Nom du module : {}</translation>
    </message>
    <message>
        <location filename="../libs/plugins/_template.py" line="73"/>
        <source>Generic plugin, does nothing.</source>
        <translation>Plugin générique : ne fait rien.</translation>
    </message>
    <message>
        <location filename="../libs/plugins/image.py" line="44"/>
        <source>Open Image File</source>
        <translation>Ouvrir un fichier d&apos;image</translation>
    </message>
    <message>
        <location filename="../libs/plugins/video.py" line="249"/>
        <source>Images (*.png *.xpm *.jpg *.jpeg)</source>
        <translation>Images (*.png *.xpm *.jpg *.jpeg)</translation>
    </message>
    <message>
        <location filename="../libs/plugins/video.py" line="179"/>
        <source>Insert a video</source>
        <translation>Insérer une vidéo</translation>
    </message>
    <message>
        <location filename="../libs/plugins/video.py" line="197"/>
        <source>Open Video File</source>
        <translation>Ouvrir un fichier vidéo</translation>
    </message>
    <message>
        <location filename="../libs/plugins/video.py" line="44"/>
        <source>Videos (*.webm *.mkv *.ogg *.ogv *.avi *.mov *.wmv *.mp4 *.m4v *.mpg)</source>
        <translation type="obsolete">Vidéos (*.webm *.mkv *.ogg *.ogv *.avi *.mov *.wmv *.mp4 *.m4v *.mpg)</translation>
    </message>
    <message>
        <location filename="../libs/plugins/video.py" line="197"/>
        <source>Videos (*.webm *.mkv *.ogv *.avi *.mov *.wmv *.mp4 *.m4v *.mpg)</source>
        <translation>Vidéos (*.webm *.mkv *.ogv *.avi *.mov *.wmv *.mp4 *.m4v *.mpg)</translation>
    </message>
    <message>
        <location filename="../libs/plugins/video.py" line="249"/>
        <source>Open Poster Image File</source>
        <translation>Ouvrir un fichier pour l&apos;image de présentation</translation>
    </message>
    <message>
        <location filename="../libs/plugins/video.py" line="92"/>
        <source>Unnamed</source>
        <translation>Sans nom</translation>
    </message>
    <message>
        <location filename="../libs/plugins/audio.py" line="166"/>
        <source>Insert an audio track</source>
        <translation>Insérer un lecteur audio</translation>
    </message>
    <message>
        <location filename="../libs/plugins/audio.py" line="184"/>
        <source>Open Audio File</source>
        <translation>Ouvrir un fichier audio</translation>
    </message>
    <message>
        <location filename="../libs/plugins/audio.py" line="184"/>
        <source>Audios (*.wav *.ogg *.mp3 *.m4a *.wma)</source>
        <translation>Audios (*.wav *.ogg *.mp3 *.m4a *.wma)</translation>
    </message>
    <message>
        <location filename="../libs/plugins/image.py" line="44"/>
        <source>Images (*.gif *.png *.xpm *.jpg *.jpeg)</source>
        <translation>Images (*.gif *.png *.xpm *.jpg *.jpeg)</translation>
    </message>
    <message>
        <location filename="../libs/plugins/anim_rotate_x.py" line="18"/>
        <source>Make a rotation around X axis</source>
        <translation>Crée une rotation autour de l&apos;axe X</translation>
    </message>
    <message>
        <location filename="../libs/plugins/anim_rotate_x.py" line="18"/>
        <source>Rotate X</source>
        <translation>Rotation X</translation>
    </message>
    <message>
        <location filename="../libs/plugins/anim_rotate_y.py" line="18"/>
        <source>Make a rotation around Y axis</source>
        <translation>Créer une rotation autour de l&apos;axe Y</translation>
    </message>
    <message>
        <location filename="../libs/plugins/anim_rotate_y.py" line="18"/>
        <source>Rotate Y</source>
        <translation>Rotation Y</translation>
    </message>
    <message>
        <location filename="../libs/plugins/anim_rotate_z.py" line="18"/>
        <source>Make a rotation around Z axis</source>
        <translation>Créer une rotation autour de l&apos;axe Z</translation>
    </message>
    <message>
        <location filename="../libs/plugins/anim_rotate_z.py" line="18"/>
        <source>Rotate Z</source>
        <translation>Rotation Z</translation>
    </message>
    <message>
        <location filename="../libs/plugins/anim_rotate_y_infinite.py" line="18"/>
        <source>Make an oscillation around Y axis</source>
        <translation>Créer une oscillation autour de l&apos;axe Y</translation>
    </message>
    <message>
        <location filename="../libs/plugins/anim_rotate_y_infinite.py" line="18"/>
        <source>Oscillate Y</source>
        <translation>Osciller Y</translation>
    </message>
    <message>
        <location filename="../libs/plugins/anim_scale.py" line="18"/>
        <source>Make a scaling animation</source>
        <translation>Créer une animation zoom</translation>
    </message>
    <message>
        <location filename="../libs/plugins/anim_scale.py" line="18"/>
        <source>Scale</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../libs/plugins/title.py" line="55"/>
        <source>Make a title</source>
        <translation>Créer un titre</translation>
    </message>
    <message>
        <location filename="../libs/plugins/title.py" line="71"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../libs/plugins/bold.py" line="18"/>
        <source>Bold</source>
        <translation>Gras</translation>
    </message>
    <message>
        <location filename="../libs/plugins/_font_template.py" line="67"/>
        <source>REPLACE ME</source>
        <translation>REMPLACEZ MOI</translation>
    </message>
    <message>
        <location filename="../libs/plugins/_template.py" line="67"/>
        <source>Module name: {}, icon: {}</source>
        <translation>Nom du module : {}, icône : {}</translation>
    </message>
    <message>
        <location filename="../libs/plugins/italic.py" line="18"/>
        <source>Italic</source>
        <translation>Italique</translation>
    </message>
    <message>
        <location filename="../libs/plugins/tt.py" line="18"/>
        <source>Typewriter text</source>
        <translation>Texte « machine »</translation>
    </message>
    <message>
        <location filename="../libs/plugins/underlined.py" line="18"/>
        <source>Underlined</source>
        <translation>Souligné</translation>
    </message>
</context>
<context>
    <name>self.parent</name>
    <message>
        <location filename="../libs/plugins/_template.py" line="27"/>
        <source>Insert some template</source>
        <translation type="obsolete">Insère un exemple</translation>
    </message>
    <message>
        <location filename="../libs/plugins/_template.py" line="49"/>
        <source>Module name: {}</source>
        <translation type="obsolete">Nome du module : {}</translation>
    </message>
    <message>
        <location filename="../libs/plugins/_template.py" line="55"/>
        <source>Generic plugin, does nothing.</source>
        <translation type="obsolete">Plugin générique : ne fait rien.</translation>
    </message>
    <message>
        <location filename="../libs/plugins/image.py" line="26"/>
        <source>Insert an image</source>
        <translation type="obsolete">Insérer une image</translation>
    </message>
    <message>
        <location filename="../libs/plugins/image.py" line="35"/>
        <source>Open Image File</source>
        <translation type="obsolete">Ouvrir un fichier d&apos;image</translation>
    </message>
    <message>
        <location filename="../libs/plugins/image.py" line="35"/>
        <source>Images (*.png *.xpm *.jpg *.jpeg)</source>
        <translation type="obsolete">Images (*.png *.xpm *.jpg *.jpeg)</translation>
    </message>
</context>
</TS>
