### Configuration de l'export PDF

* si votre présentation comporte des notes, vous pouvez choisir de les imprimer ou non dans le fichier ;
* la procédure passant par la création d'un fichier html temporaire, vous pouvez demander à l'ouvrir dans votre navigateur, ce qui vous donnera plus de possibilités de configuration lors de l'impression (numérotation des pages, orientation, etc) ;
* finalement vous pouvez même demander à ne pas créer le fichier PDF.
