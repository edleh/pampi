







( function( document, window ) {
    "use strict";

    // ICI : throttles mouse wheel navigation
    var lastMouseWheelStep = 0;



    document.addEventListener( "impress:init", function( event ) {

        var api = event.detail.api;

        document.addEventListener( "keyup", function( event ) {

            if ( event.shiftKey || event.altKey || event.ctrlKey || event.metaKey ) {
                return;
            }

            if ( event.keyCode === 9 ||
               ( event.keyCode >= 32 && event.keyCode <= 34 ) ||
               ( event.keyCode >= 37 && event.keyCode <= 40 ) ) {
                switch ( event.keyCode ) {
                    // ICI : modifs pour overview
                    case 33: // Page up
                    case 38: // Up
                             api.goto("overview")
                             break;
                    case 37: // Left
                             api.prev();
                             break;
                    case 9:  // Tab
                    case 32: // Space
                    case 34: // Page down
                    case 39: // Right
                    case 40: // Down
                             api.next();
                             break;
                }

                event.preventDefault();
            }
        }, false );

        // ICI : delegated handler for double clicking
        document.addEventListener( "dblclick", function ( event ) {
            api.goto( "overview" );
            event.preventDefault();
        }, false);
        
        // ICI : delegated handler for mouseWheel
        document.addEventListener( "mousewheel", function ( event ) {
            var target = event.target;
            if (typeof target.tagName !== "undefined") {
                while ( ( !target.classList.contains( "no-mousewheel" ) ) &&
                        ( target !== document.documentElement ) ) {
                    target = target.parentNode;
                }
            }
            if ( (typeof target.tagName === "undefined") || 
                ( !target.classList.contains( "no-mousewheel" ) ) ) {
                var delta = event.detail || -event.wheelDelta;
                if ( Date.now() - lastMouseWheelStep > 600 )  {
                    lastMouseWheelStep = Date.now();
                    if ( delta < 0 )
                        api.next();
                    else
                        api.prev();
                    event.preventDefault();
                }
            }
        }, false);

        // ICI : delegated handler for mouseWheel (FireFox)
        document.addEventListener("DOMMouseScroll", function ( event ) {
           var target = event.target;
            if (typeof target.tagName !== "undefined") {
                while ( ( !target.classList.contains( "no-mousewheel" ) ) &&
                        ( target !== document.documentElement ) ) {
                    target = target.parentNode;
                }
            }
            if ( (typeof target.tagName === "undefined") || 
                ( !target.classList.contains( "no-mousewheel" ) ) ) {
                var delta = event.detail || -event.wheelDelta;
                if ( Date.now() - lastMouseWheelStep > 600 )  {
                    lastMouseWheelStep = Date.now();
                    if ( delta < 0 )
                        api.next();
                    else
                        api.prev();
                    event.preventDefault();
                }
            }
        }, false);


    }, false );

} )( document, window );





// Initialize impress.js
impress().init();


// Set up the help-box
var helpdiv = window.document.getElementById('hovercraft-help');

if (window.top!=window.self) {
    // This is inside an iframe, so don't show the help.
    helpdiv.className = "disabled";

} else {
    // Install a funtion to toggle help on and off.
    var help = function() {
        if(helpdiv.className == 'hide')
            helpdiv.className = 'show';
        else
            helpdiv.className = 'hide';
    };
    impressConsole().registerKeyEvent([72], help, window);

    // The help is by default shown. Hide it after five seconds.
    setTimeout(function () {
        var helpdiv = window.document.getElementById('hovercraft-help');
        if(helpdiv.className != 'show')
            helpdiv.className = 'hide';
    }, 5000);
}
