<!--
HEADER FOR PAMPI CONFIG
TITLE:vis.js
COLOR:#c5daff
OTHER TOOLS:|Vis
-->




#  {.step data-y=-5000 data-scale=4}

## Exemples d'utilisation \
de [vis.js](http://visjs.org)

Certaines fonctionnalités \
ne seront visibles que dans le navigateur.












# {.step .no-mousewheel .slide data-x=-0.0 data-y=-2000 data-z=-2000.0 data-rotate-y=0.0}

###### Le **step** doit avoir la propriété **.no-mousewheel** \
pour que la molette permette de zoomer


<div id="visualization1"></div>
<script>
    var container = document.getElementById('visualization1');
  var items = [
    {x: '2014-06-11', y: 10},
    {x: '2014-06-12', y: 25},
    {x: '2014-06-13', y: 30},
    {x: '2014-06-14', y: 10},
    {x: '2014-06-15', y: 15},
    {x: '2014-06-16', y: 30}
  ];
  var dataset = new vis.DataSet(items);
  var options = {
    start: '2014-06-10',
    end: '2014-06-18'
  };
  var graph2d = new vis.Graph2d(container, dataset, options);
</script>

















# {.step .no-mousewheel .slide data-x=1902.0 data-y=-1000 data-z=-618.0 data-rotate-y=-72.0}
## un autre exemple

<div id="visualization2"></div>
<script>
    // create a dataSet with groups
    var names = ['SquareShaded', 'Bargraph', 'Blank', 'CircleShaded'];
    var groups = new vis.DataSet();
        groups.add({
        id: 0,
        content: names[0],
        options: {
            drawPoints: {
                style: 'square' // square, circle
            },
            shaded: {
                orientation: 'bottom' // top, bottom
            }
        }});

    groups.add({
        id: 1,
        content: names[1],
        options: {
            style:'bar'
        }});

    groups.add({
        id: 2,
        content: names[2],
        options: {drawPoints: false}
    });

    groups.add({
        id: 3,
        content: names[3],
        options: {
            drawPoints: {
                style: 'circle' // square, circle
            },
            shaded: {
                orientation: 'top' // top, bottom
            }
        }});

  var container = document.getElementById('visualization2');
  var items = [
    {x: '2014-06-13', y: 60},
    {x: '2014-06-14', y: 40},
    {x: '2014-06-15', y: 55},
    {x: '2014-06-16', y: 40},
    {x: '2014-06-17', y: 50},
    {x: '2014-06-13', y: 30, group: 0},
    {x: '2014-06-14', y: 10, group: 0},
    {x: '2014-06-15', y: 15, group: 1},
    {x: '2014-06-16', y: 30, group: 1},
    {x: '2014-06-17', y: 10, group: 1},
    {x: '2014-06-18', y: 15, group: 1},
    {x: '2014-06-19', y: 52, group: 1},
    {x: '2014-06-20', y: 10, group: 1},
    {x: '2014-06-21', y: 20, group: 2},
    {x: '2014-06-22', y: 60, group: 2},
    {x: '2014-06-23', y: 10, group: 2},
    {x: '2014-06-24', y: 25, group: 2},
    {x: '2014-06-25', y: 30, group: 2},
    {x: '2014-06-26', y: 20, group: 3},
    {x: '2014-06-27', y: 60, group: 3},
    {x: '2014-06-28', y: 10, group: 3},
    {x: '2014-06-29', y: 25, group: 3},
    {x: '2014-06-30', y: 30, group: 3}
  ];

  var dataset = new vis.DataSet(items);
  var options = {
      defaultGroup: 'ungrouped',
      legend: true,
      start: '2014-06-10',
      end: '2014-07-04'
  };
  var graph2d = new vis.Graph2d(container, dataset, groups, options);
</script>





















# {.step .no-mousewheel data-x=1176.0 data-y=0 data-z=1618.0 data-rotate-y=-144.0}
##  un graphique en 3D

Là ça fonctionne aussi dans l'interface de PAMPI

<div id="visualization3" style="background: white"></div>
<script>
    // Create and populate a data table.
    var data = new vis.DataSet();
    // create some nice looking data with sin/cos
    var counter = 0;
    var steps = 50;  // number of datapoints will be steps*steps
    var axisMax = 314;
    var axisStep = axisMax / steps;
    for (var x = 0; x < axisMax; x+=axisStep) {
        for (var y = 0; y < axisMax; y+=axisStep) {
            var value = (Math.sin(x/50) * Math.cos(y/50) * 50 + 50);
            data.add({id:counter++,x:x,y:y,z:value,style:value});
        }
    }

    // specify options
    var options = {
        width:  '100%',
        height: '500px',
        style: 'surface',
        showPerspective: true,
        showGrid: true,
        showShadow: false,
        keepAspectRatio: true,
        verticalRatio: 0.5
    };

    // Instantiate our graph object.
    var container = document.getElementById('visualization3');
    var graph3d = new vis.Graph3d(container, data, options);
</script>































# {.step .no-mousewheel .no-scale data-x=-1176.0 data-y=1000 data-z=1618.0 data-rotate-y=-216.0}

Il y a un bug pour déplacer les points : il faut les attraper à droite de leur position réelle. \
Le décalage se réduit quand on zoome.


<div id="mynetwork" style="width: 500px; height: 400px; margin: auto; background: lightgray"></div>
<script>
    var len = undefined;

    var nodes = [{id: 0, label: "0", group: 0},
        {id: 1, label: "1", group: 0},
        {id: 2, label: "2", group: 0},
        {id: 3, label: "3", group: 1},
        {id: 4, label: "4", group: 1},
        {id: 5, label: "5", group: 1},
        {id: 6, label: "6", group: 2},
        {id: 7, label: "7", group: 2},
        {id: 8, label: "8", group: 2},
        {id: 9, label: "9", group: 3},
        {id: 10, label: "10", group: 3},
        {id: 11, label: "11", group: 3},
        {id: 12, label: "12", group: 4},
        {id: 13, label: "13", group: 4},
        {id: 14, label: "14", group: 4},
        {id: 15, label: "15", group: 5},
        {id: 16, label: "16", group: 5},
        {id: 17, label: "17", group: 5},
        {id: 18, label: "18", group: 6},
        {id: 19, label: "19", group: 6},
        {id: 20, label: "20", group: 6},
        {id: 21, label: "21", group: 7},
        {id: 22, label: "22", group: 7},
        {id: 23, label: "23", group: 7},
        {id: 24, label: "24", group: 8},
        {id: 25, label: "25", group: 8},
        {id: 26, label: "26", group: 8},
        {id: 27, label: "27", group: 9},
        {id: 28, label: "28", group: 9},
        {id: 29, label: "29", group: 9}
    ];
    var edges = [{from: 1, to: 0},
        {from: 2, to: 0},
        {from: 4, to: 3},
        {from: 5, to: 4},
        {from: 4, to: 0},
        {from: 7, to: 6},
        {from: 8, to: 7},
        {from: 7, to: 0},
        {from: 10, to: 9},
        {from: 11, to: 10},
        {from: 10, to: 4},
        {from: 13, to: 12},
        {from: 14, to: 13},
        {from: 13, to: 0},
        {from: 16, to: 15},
        {from: 17, to: 15},
        {from: 15, to: 10},
        {from: 19, to: 18},
        {from: 20, to: 19},
        {from: 19, to: 4},
        {from: 22, to: 21},
        {from: 23, to: 22},
        {from: 22, to: 13},
        {from: 25, to: 24},
        {from: 26, to: 25},
        {from: 25, to: 7},
        {from: 28, to: 27, shadow:{color:'rgb(0,255,0)'}},
        {from: 29, to: 28},
        {from: 28, to: 0}
    ]

    // create a network
    var container = document.getElementById('mynetwork');
    var data = {
        nodes: nodes,
        edges: edges
    };
    var options = {
        nodes: {
            shape: 'dot',
            size: 30,
            font: {
                size: 32
            },
            borderWidth: 2,
            shadow:true
        },
        edges: {
            width: 2,
            shadow:true
        }
    };
    network = new vis.Network(container, data, options);
</script>


























# {.step .no-mousewheel .slide data-x=-1902.0 data-y=2000 data-z=-618.0 data-rotate-y=-288.0}
## un dernier exemple

<div id="visualization5"></div>
<script>
    // create a dataSet with groups
    var names = ['top', 'bottom', 'zero', 'none', 'group', 'none'];
    var groups = new vis.DataSet();
    groups.add({
        id: 0,
        content: names[0],
        options: {
            shaded: {
                orientation: 'top'
            }
        }});

    groups.add({
        id: 1,
        content: names[1],
        options: {
            shaded: {
                orientation: 'bottom'
            }
        }});

    groups.add({
        id: 2,
        content: names[2],
        options: {
            shaded: {
                orientation: 'zero'
            }
        }});

    groups.add({
        id: 3,
        options: {
            excludeFromLegend: true
        }
    });

    groups.add({
        id: 4,
        content: names[4],
        options: {
            shaded: {
                orientation: 'group',
                groupId: '3'
            }
        }
    });

    groups.add({
        id: 5,
        content: names[5]
    });

  var container = document.getElementById('visualization5');
    var items = [
        {x: '2014-06-11', y: 0, group: 0},
        {x: '2014-06-12', y: 15, group: 0},
        {x: '2014-06-13', y: -15, group: 0},
        {x: '2014-06-14', y: 0, group: 0},
        {x: '2014-06-15', y: 0, group: 1},
        {x: '2014-06-16', y: 15, group: 1},
        {x: '2014-06-17', y: -15, group: 1},
        {x: '2014-06-18', y: 0, group: 1},
        {x: '2014-06-19', y: 0, group: 2},
        {x: '2014-06-20', y: 15, group: 2},
        {x: '2014-06-21', y: -15, group: 2},
        {x: '2014-06-22', y: 0, group: 2},
        {x: '2014-06-23', y: -2, group: 3},
        {x: '2014-06-24', y: 13, group: 3},
        {x: '2014-06-25', y: -17, group: 3},
        {x: '2014-06-26', y: -2, group: 3},
        {x: '2014-06-23', y: 2, group: 4},
        {x: '2014-06-24', y: 17, group: 4},
        {x: '2014-06-25', y: -13, group: 4},
        {x: '2014-06-26', y: 2, group: 4},
        {x: '2014-06-27', y: 0, group: 5},
        {x: '2014-06-28', y: 15, group: 5},
        {x: '2014-06-29', y: -15, group: 5},
        {x: '2014-06-30', y: 0, group: 5}
    ];

  var dataset = new vis.DataSet(items);
  var options = {
      legend: true,
      start: '2014-06-07',
      end: '2014-07-03'
  };
  var graph2d = new vis.Graph2d(container, dataset, groups, options);
</script>
















# {#overview .step data-x=0 data-y=0 data-scale=8}
