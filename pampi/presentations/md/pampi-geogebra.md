<!--
HEADER FOR PAMPI CONFIG
TITLE:GeoGebra
COLOR:#f1f6ff
OTHER TOOLS:|GeoGebra
-->


#  {.step data-x=-2000 data-y=-2000 data-scale=4}

## TEST D'OUTILS JS








# {.step data-x=0 data-y=2000}
## GeoGebra

[http://wiki.geogebra.org/en/Reference:Math_Apps_Embedding](http://wiki.geogebra.org/en/Reference:Math_Apps_Embedding)



# {.step .no-scale data-x=1000 data-y=2000}

<div style="width:100%;height:550px" id="gg-1"></div>
<script>
var applet1 = new GGBApplet({
    filename: "data/pampi-help/test.ggb",
    "showtoolbar":true, 
    "language":"fr", 
    "customToolBar":"0|1|2"}, 
    true);
window.onload = function() {
    applet1.inject('gg-1', 'preferHTML5');
}
</script>




# {.step data-x=2000 data-y=2000}

<!--
<div style="width:100%;height:550px;display:block" id="gg-2"></div>
<script>
var applet2 = new GGBApplet({
    filename: "data/pampi-help/test.ggb",
    "showtoolbar":true, 
    "language":"fr", 
    "customToolBar":"0|1|2"}, 
    true);
window.onload = function() {
    applet2.inject('gg-2', 'preferHTML5');
}
</script>
-->

<!--
# {.step data-x=3000 data-y=2000}
## 14
![](data/pampi-help/splash.png)


# {.step data-x=4000 data-y=2000}
## 15
![](data/pampi-help/splash.png)
-->
















# {#overview .step data-x=0 data-y=0 data-scale=10}
