<!--
HEADER FOR PAMPI CONFIG
TITLE:D3.js
COLOR:#f1f6ff
OTHER TOOLS:|D3
-->


#  {.step .slide data-x=-2000 data-y=-2000 data-scale=4 style="background: Tan"}

## D3.JS

[https://d3js.org](https://d3js.org)

Bibliothèque graphique JavaScript qui permet l'affichage de données numériques sous une forme graphique et dynamique.










# {.step .slide data-x=0 data-y=0 style="background: PapayaWhip"}
## avec C3.js

[http://c3js.org](http://c3js.org)






# {.step .slide data-x=1000 data-y=0}

<div id="c3-1" class="graph"></div>
<script>
var chart = c3.generate({
    bindto: '#c3-1',
    data: {
        columns: [
            ['data1', 30, 200, 100, 400, 150, 250],
            ['data2', 50, 20, 10, 40, 15, 25]
        ],
    },
});
</script>





# {.step .slide data-x=2000 data-y=0}

<div id="c3-2" class="graph"></div>
<script>
var chart = c3.generate({
    bindto: '#c3-2',
    data: {
        columns: [
            ["setosa", 0.2, 0.2, 2.2],
            ["versicolor", 1.4, 1.5, 1.5],
            ["virginica", 2.5, 1.9, 2.1],
        ],
        type : 'pie',
    },
});
</script>






# {.step .slide data-x=3000 data-y=0}

<div id="c3-3" class="graph no-mousewheel"></div>
<script>
var chart = c3.generate({
    bindto: '#c3-3',
    data: {
        columns: [
            ['sample', 30, 200, 100, 400, 150, 250, 150, 200, 170, 240, 350, 150, 100, 400, 150, 250, 150, 200, 170, 240, 100, 150, 250, 150, 200, 170, 240, 30, 200, 100, 400, 150, 250, 150, 200, 170, 240, 350, 150, 100, 400, 350, 220, 250, 300, 270, 140, 150, 90, 150, 50, 120, 70, 40]
        ]
    },
    zoom: {
        enabled: true
    }
});
</script>







# {.step data-x=4000 data-y=0}

<div id="c3-4" class="graph"></div>
<script>
var chart = c3.generate({
    bindto: '#c3-4',
    data: {
        x: 'x',
        columns: [
            ['x', -50, 0, 50, 100, 150, 400],
            ['data1', 30, 200, 100, 400, 150, 250],
            ['data2', 130, 300, 200, 300, 250, 450]
        ],
        type: 'spline'
    }
});
</script>
































# {.step .slide data-x=0 data-y=1000 style="background: PapayaWhip"}
## avec xkcd.js

[http://dan.iel.fm/xkcd](http://dan.iel.fm/xkcd)






# {.step .slide data-x=1000 data-y=1000}

### XKCD

<div id="xkcd-1" class="xkcd"></div>
<script>

    // Generate some data.
    function f1 (x) {
        return Math.exp(-0.5 * (x - 1) * (x - 1)) * Math.sin(x + 0.2) + 0.05;
    }

    function f2 (x) {
        return 0.5 * Math.cos(x - 0.5) + 0.1;
    }

    var xmin = -10,
        xmax = 10,
        N = 100,
        data = d3.range(xmin + 3, xmax - 3, (xmax - xmin) / N).map(function (d) {
            return {x: d, y: f1(d)};
        })
        data2 = d3.range(xmin, xmax, (xmax - xmin) / N).map(function (d) {
            return {x: d, y: f2(d)};
        });

    // Build the plot.
    var parameters = {
        title: "The most important graph ever made", 
        xlabel: "x", 
        ylabel: "y", 
        width: 600, 
        height: 300, 
        magnitude: 0.005, 
        };
    var plot = xkcdplot();
    plot("#xkcd-1", parameters);

    // Add the lines.
    plot.plot(data);
    plot.plot(data2, {stroke: "red"});

    // Render the image.
    plot.xlim([-11, 11]).draw();

</script>

azerty













# {.step data-x=2000 data-y=1000}

### XKCD bis

<div id="xkcd-2" class="xkcd"></div>
<script>

    // Generate some data.
    function f1 (x) {
        return 0.5 * Math.sin(x - 0.5) + 0.1;
    }

    function f2 (x) {
        return 0.5 * Math.cos(x - 0.5) + 0.1;
    }

    var xmin = -10,
        xmax = 10,
        N = 100,
        data = d3.range(xmin + 3, xmax + 3, (xmax - xmin) / N).map(function (d) {
            return {x: d, y: f1(d)};
        })
        data2 = d3.range(xmin, xmax, (xmax - xmin) / N).map(function (d) {
            return {x: d, y: f2(d)};
        });

    // Build the plot.
    var parameters = {
        title: "Un joli graphique", 
        xlabel: "axe des x", 
        ylabel: "axe des y", 
        width: 600, 
        height: 300, 
        magnitude: 0.01, 
        };
    var plot = xkcdplot();
    plot("#xkcd-2", parameters);

    // Add the lines.
    plot.plot(data);
    plot.plot(data2, {stroke: "red"});

    // Render the image.
    plot.xlim([-11, 11]).draw();

</script>



















# {#overview .step data-x=0 data-y=0 data-scale=10}
