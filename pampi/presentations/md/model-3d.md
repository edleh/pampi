<!--
HEADER FOR PAMPI CONFIG
TITLE:titre
-->


# {.step data-x=2000 data-z=0 data-rotate-y=0}

## 0

![](data/pampi-help/splash.png)



# {.step data-x=1000 data-z=1732 data-rotate-y=-60}

## 1

![](data/pampi-help/splash.png)



# {.step data-x=-1000 data-z=1732 data-rotate-y=-120}

## 2

![](data/pampi-help/splash.png)



# {.step data-x=-2000 data-z=0 data-rotate-y=-180}

## 3

![](data/pampi-help/splash.png)



# {.step data-x=-1000 data-z=-1732 data-rotate-y=-240}

## 4

![](data/pampi-help/splash.png)



# {.step data-x=1000 data-z=-1732 data-rotate-y=-300}

## 5

![](data/pampi-help/splash.png)


<!--
import math

n = 6
r = 2000
for i in range(n):
    a = -i * 2 * math.pi / n
    d = round(180 * a / math.pi)
    x = round(r * math.cos(a))
    y = - round(r * math.sin(a))
    #print(i + 1, d, x, y)
    print('# {3}.step data-x={0} data-z={1} data-rotate-y={2}{4}\n'.format(x, y, d, '{', '}'))
    print('## {0}\n'.format(i))
    print('![](data/pampi-help/splash.png)\n\n\n')
-->




# {#overview .step data-x=0 data-y=0 data-scale=4}
