"""
Structure minimale pour un plugin destiné à Pampi
"""

from PyQt5 import QtCore, QtWidgets, QtGui

import os

"""
Il faut activer la ligne qui suit dans les vrais plugins, et
faire en sorte que que la classe Plugin hérite de Template
après ça, il suffit de surcharger peu de choses : l'icône, le nom
et la méthode _run.
"""
# from ._template import Plugin as Template

class Plugin:
    """
    Le constructeur prend comme paramètre la fenêtre principale
    de l'application. Il doit définir les propriété name et action
    @param mainWindow la fenêtre pincipale
    """
    def __init__(
            self, mainWindow,
            name = "template",
            iconPath = '/usr/share/icons/gnome/48x48/emotes/face-cool.png',
            title = None
    ):
        self.parent = mainWindow
        self._name = name
        self.presentationsDir = self.parent.presentationsDir
        self.dataDir = os.path.join(self.presentationsDir, "data")
        self.iconPath = iconPath
        if not title:
            title = QtCore.QCoreApplication.translate("main","Insert some template")
        self._initAction(iconPath, title)
        return

    def simplifyDir(self, path):
        """
        Retire le début d'un chemin absolu vers une ressource, afin de
        renvoyer un chemin relatif commençant au répertoire des présentations
        """
        return path.replace(self.presentationsDir, "")[1:]
    
    def _initAction(self, iconFile, hint=None):
        icon = QtGui.QIcon(iconFile)
        self._action = QtWidgets.QAction(icon, self._name, self.parent)
        self._action.triggered.connect(lambda: self._run())
        if hint:
            self._action.setToolTip(hint)
            self._action.setStatusTip(hint)
        return
    
    @property
    def name(self):
        return self._name

    @property
    def action(self):
        return self._action

    def __str__(self):
        return QtCore.QCoreApplication.translate("main","Module name: {}, icon: {}").format(self.name, self.iconPath)

    def _run(self):
        """
        Fonction de rappel pour le plugin
        """
        print(QtCore.QCoreApplication.translate("main","Generic plugin, does nothing."))
        return
    
    def insertSelected(self, text):
        """
        insère un texte à la place de la sélection, puis le sélectionne
        """
        ed = self.parent.markdownEditor
        cursor = ed.textCursor()
        start = cursor.selectionStart()
        cursor.insertText(text)
        end = cursor.position()
        cursor.setPosition(start)
        cursor.setPosition(end, QtGui.QTextCursor.KeepAnchor)
        ed.setTextCursor(cursor)
        return

    def getSelected(self):
        """
        enlèverécupère le texte sélectionné et renvoie sa valeur
        """
        ed = self.parent.markdownEditor
        cursor=ed.textCursor()
        fragment = cursor.selection().toPlainText()
        return fragment
        
class MyTimeEdit(QtWidgets.QDateTimeEdit):
    def __init__(self, parent=None):
        super(MyTimeEdit, self).__init__(parent)
        self.setDisplayFormat("hh:mm:ss.zzz")
        self.setSelectedSection(self.MinuteSection)
        return

    def seconds(self):
        """
        renvoie un flottant, le temps en secondes
        """
        t = self.time()
        return 3600*t.hour() + 60*t.minute() + t.second() + t.msec() / 1000
