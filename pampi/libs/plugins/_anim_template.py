"""
Un plugin destiné à Pampi, pour insérer une animation dans l'éditeur
à la sélection courante du curseur. La classe Plugin concerne une 
"animation abstraite", il faut la spécialiser
"""

from PyQt5 import QtCore, QtWidgets

import os, re

from ._template import Plugin as Template
from .ui_anim import *

anim_template = """\
<{container} {properties}>
{contents}
</{container}>
"""

anim_template1 = """<{container} {properties}>{contents}</{container}>"""

anim_pattern = re.compile(r"""[\s\n]*<(?P<container>div|span)\s+(?P<properties>[^>]*)>[\s\n]*(?P<contents>.*)[\s\n]*</(?P=container)>""", re.M|re.S)

class AnimDialog(QtWidgets.QDialog, Ui_Dialog):
    def __init__(self, parent = None, container="", properties="", contents=""):
        super(AnimDialog, self).__init__(parent.parent)
        self.setupUi(self)
        self.containerEdit.setText(container)
        self.propertiesEdit.setText(properties)
        self.contentsEdit.setPlainText(contents.strip())
        if "div" in container:
            self.globalButton.setChecked(True)
        else:
            self.textButton.setChecked(True)
        self.globalButton.toggled.connect(self.changeContainer)
        return

    def changeContainer(self, state):
        if state:
            self.containerEdit.setText('div')
        else:
            self.containerEdit.setText('span')
        return
    

class Plugin(Template):
    """
    Le constructeur prend comme paramètre la fenêtre principale
    de l'application. Il doit définir les propriété name et action
    @param mainWindow la fenêtre pincipale
    """
    def __init__(
            self, mainWindow,
            animation_type = "anim-undefined",
            iconPath = '/usr/share/icons/gnome/48x48/emotes/face-cool.png',
            title = None,
            name = "anim-undefined",
    ):
        super(Plugin, self).__init__(
            mainWindow,
            iconPath = iconPath,
            name="anim-undefined",
            title = title
        )
        self.animation_type = animation_type
        return
    
    def _run(self):
        """
        Fonction de rappel pour le plugin
        """
        fragment = self.getSelected()
        lines = fragment.split("\n")
        m = anim_pattern.search(fragment)
        # si ça matche, les groupes suivants sont définis :
        # "container", "properties", "contents"
        container  = "div"
        properties = 'class="{}"'.format(self.animation_type)
        contents   = fragment
        if not fragment:
            contents = "..."
        if m:
            container  = m.group("container")
            # la propriété doit toujours venir du type de plugin
            # properties = m.group("properties")
            contents   = m.group("contents")
        self.ad = AnimDialog(self, container, properties, contents)
        ok = self.ad.exec_()
        if ok:
            contents = self.ad.contentsEdit.toPlainText()
            container = self.ad.containerEdit.text()
            properties = self.ad.propertiesEdit.text()
            # on injecte le résultat dans l'éditeur
            # si la zone sélectionnées est non vide et tient sur une ligne
            template = anim_template
            if len(lines) == 1 and fragment.strip() != "":
                template = anim_template1
            self.insertSelected(template.format(
                contents = contents,
                container = container,
                properties = properties
            ))
        return
        
    
